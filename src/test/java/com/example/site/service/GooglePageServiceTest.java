package com.example.site.service;

import com.example.site.config.MvcConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {MvcConfig.class})
@WebAppConfiguration
public class GooglePageServiceTest {

    @Autowired GooglePageService googlePageService;


    @Test
    public void testGetGooglePage() {
        System.out.println(googlePageService.getGooglePage());
    }


}
