package com.example.site.dao;

import com.example.site.config.DataBaseConfig;
import com.example.site.model.entity.Human;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataBaseConfig.class})
@Transactional
public class HumanDaoTest {

    @Autowired
    HumanDao humanDao;

    /*@Test
    public void testInit() {
        List<Human> humans = (List<Human>) humanDao.findAll();

        for (Human human : humans) {
            System.out.println(human.toString());
        }

    }*/

    /*@Test
    @Rollback(value = false)
    public void testSave() {
        Human human = new Human("Petya", "Ivanov", 22, null);
        humanDao.save(human);
    }*/


    /*@Test
    public void testFindByaAge() {
        List<Human> humans = humanDao.findAllByAge(22);

        for (Human human : humans) {
            System.out.println(human.toString());
        }
    }*/

    @Test
    public void testFindByaLastName() {
        List<Human> humans = humanDao.findAllByAgeLessThan(20);

        for (Human human : humans) {
            System.out.println(human.toString());
        }
    }

}
