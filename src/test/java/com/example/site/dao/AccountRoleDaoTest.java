package com.example.site.dao;

import com.example.site.config.DataBaseConfig;
import com.example.site.model.entity.Account;
import com.example.site.model.entity.Role;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataBaseConfig.class})
@Transactional
public class AccountRoleDaoTest {

    @Autowired AccountDao accountDao;
    @Autowired RoleDao roleDao;


    /*@Test
    @Rollback(value = false)
    public void testRoleSave() {
        Role admin = new Role("ROLE_ADMIN", null);
        Role user = new Role("ROLE_USER", null);

        roleDao.save(admin);
        roleDao.save(user);

        Iterable<Role> roles = roleDao.findAll();
        System.out.println("saved");
    }*/

    /*@Test
    @Rollback(value = false)
    public void testAccountSave() {
        Iterable<Role> roles = roleDao.findAll();
        Set<Role> allRoles = new HashSet<Role>();
        Set<Role> vasyaRoles = new HashSet<Role>();
        for (Role role : roles) {
            allRoles.add(role);
            if (role.getName() == "ROLE_USER") {
                vasyaRoles.add(role);
            }
        }

        Account admin = new Account("admin@mail.ru", "1234", true, allRoles);
        Account vasya = new Account("vasya@mail.ru", "333", true, vasyaRoles);

        accountDao.save(admin);
        accountDao.save(vasya);

        Iterable<Account> accounts = accountDao.findAll();

        System.out.println("saved");
    }*/

    @Test
    @Rollback(value = false)
    public void testAddRole() {
        Account vasya = accountDao.findOneByEmail("vasya@mail.ru");
        Role user = roleDao.findOneByName("ROLE_USER");
        vasya.addRole(user);

        accountDao.save(vasya);
    }



}
