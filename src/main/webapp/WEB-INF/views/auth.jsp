<%@page contentType="text/html" pageEncoding="utf-8" session="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"            prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags"          prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://www.springframework.org/tags/form"     prefix="form"%>
<!DOCTYPE html>
<html class="bg-black">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
  <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

  <title>Страница входа</title>
</head>
<body class="bg-black">
<c:if test="${not empty error}">
  <div class="error">Неверный логин или пароль</div>
</c:if>

<div class="form-box" id="login-box">

  <div class="header">Вход в Админ-панель</div>
  <form role="form" action="${loginUrl}" method="post">
    <div class="body bg-gray">
      <div class="form-group">
        <input type="text" name="j_username" class="form-control" placeholder="Логин"/>
      </div>
      <div class="form-group">
        <input type="password" name="j_password" class="form-control" placeholder="Пароль"/>
      </div>
      <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
    </div>
    <div class="footer">
      <button type="submit" class="btn bg-olive btn-block">Войти</button>
    </div>
  </form>

</div>
</body>
</html>