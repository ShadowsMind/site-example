<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Profile</title>
  <script src="${pageContext.request.contextPath}/resources/js/libs/jquery-2.1.3.min.js"></script>
</head>
<body>

<div>
  <input type="number" name="searcher">
  <div class="button-show-profile">SHOW PROFILE</div>

  <div class="profile-block">
    <p class="first-name"></p>
    <p class="last-name"></p>
    <p class="profile"></p>
  </div>

</div>
<script type="text/javascript">
  $('.button-show-profile').click(function() {
    var id = $('input[name="searcher"]').val();

    $.ajax({
      url: "api/humans/" + id,
      dataType: "json",
      type: "GET",
      success: function(data) {
        $(".first-name").text(data.firstName);
        $(".last-name").text(data.lastName);
        $(".age").text(data.age);
      }
    });

  });
</script>
</body>
</html>
