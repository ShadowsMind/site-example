package com.example.site.dao;

import com.example.site.model.entity.Apartament;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ApartamentDao extends PagingAndSortingRepository<Apartament, Long> {
}
