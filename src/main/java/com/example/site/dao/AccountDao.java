package com.example.site.dao;

import com.example.site.model.entity.Account;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface AccountDao extends PagingAndSortingRepository<Account, Long> {

    public Account findOneByEmail(String email);

}
