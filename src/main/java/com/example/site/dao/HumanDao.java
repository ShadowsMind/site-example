package com.example.site.dao;

import com.example.site.model.entity.Human;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface HumanDao extends PagingAndSortingRepository<Human, Long> {

    public List<Human> findAllByAge(Integer age);

    public List<Human> findAllByLastName(String lastName);

    public List<Human> findAllByFirstName(String firstName);

    public List<Human> findAllByAgeLessThan(Integer age);

}
