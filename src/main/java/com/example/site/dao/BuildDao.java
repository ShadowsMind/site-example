package com.example.site.dao;

import com.example.site.model.entity.Build;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface BuildDao extends PagingAndSortingRepository<Build, Long> {
}
