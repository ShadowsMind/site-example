package com.example.site.dao;

import com.example.site.model.entity.Role;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RoleDao extends PagingAndSortingRepository<Role, Long> {

    public Role findOneByName(String name);
}
