package com.example.site.model.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "human")
public class Human implements Serializable {

    public Human() {
    }

    public Human(String firstName, String lastName, Integer age, Apartament apartament) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.apartament = apartament;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "age")
    private Integer age;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "apartament_id", foreignKey = @ForeignKey(name = "fk_human__apartament"), nullable = true)
    private Apartament apartament;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Apartament getApartament() {
        return apartament;
    }

    public void setApartament(Apartament apartament) {
        this.apartament = apartament;
    }


    @Override
    public String toString() {
        return "Human{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", apartament=" + apartament +
                '}';
    }

}
