package com.example.site.model.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "build")
public class Build implements Serializable {

    public Build() {
    }

    public Build(String street, Integer floorCount) {
        this.street = street;
        this.floorCount = floorCount;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "street")
    private String street;

    @Column(name = "floor_count")
    private Integer floorCount;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Integer getFloorCount() {
        return floorCount;
    }

    public void setFloorCount(Integer floorCount) {
        this.floorCount = floorCount;
    }

    @Override
    public String toString() {
        return "Build{" +
                "id=" + id +
                ", street='" + street + '\'' +
                ", floorCount=" + floorCount +
                '}';
    }

}
