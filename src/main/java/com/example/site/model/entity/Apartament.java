package com.example.site.model.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "apartament")
public class Apartament implements Serializable {

    public Apartament() {
    }

    public Apartament(Integer roomCount, Integer area, Build build) {
        this.roomCount = roomCount;
        this.area = area;
        this.build = build;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "room_count")
    private Integer roomCount;

    @Column(name = "area")
    private Integer area;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "build_id", foreignKey = @ForeignKey(name = "fk_apartament__build"), nullable = false)
    private Build build;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getRoomCount() {
        return roomCount;
    }

    public void setRoomCount(Integer roomCount) {
        this.roomCount = roomCount;
    }

    public Integer getArea() {
        return area;
    }

    public void setArea(Integer area) {
        this.area = area;
    }

    public Build getBuild() {
        return build;
    }

    public void setBuild(Build build) {
        this.build = build;
    }


    @Override
    public String toString() {
        return "Apartament{" +
                "id=" + id +
                ", roomCount=" + roomCount +
                ", area=" + area +
                ", build=" + build +
                '}';
    }

}
