package com.example.site.model.dto;

import com.example.site.model.entity.Apartament;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

import java.io.Serializable;

public class HumanDto implements Serializable {


    public HumanDto() {
    }

    public HumanDto(Long id, String firstName, String lastName, Integer age, Apartament apartament) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.apartament = apartament;
    }

    private Long id;
    @NotEmpty
    @Length(min = 3, max = 35)
    private String firstName;
    @NotEmpty
    @Length(min = 3, max = 35)
    private String lastName;
    @Range(min = 7, max = 80)
    private Integer age;
    private Apartament apartament;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Apartament getApartament() {
        return apartament;
    }

    public void setApartament(Apartament apartament) {
        this.apartament = apartament;
    }
}
