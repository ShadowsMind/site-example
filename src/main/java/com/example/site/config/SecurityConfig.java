package com.example.site.config;

import com.example.site.service.AccountDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    AccountDetailsService accountDetailsService;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(accountDetailsService);
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.csrf().disable();

        http.authorizeRequests()
                //access for all (resources and open pages)
                .antMatchers("/resources/**").permitAll()
                //access for admin
                .antMatchers("/api**", "/api/**")
                .access("hasRole('ROLE_ADMIN')");

        http.formLogin()
                //login page
                .loginPage("/auth").failureUrl("/auth?error=true")
                //login and password for form
                .usernameParameter("j_username")
                .passwordParameter("j_password")
                        //access for login page
                .permitAll();

        http.logout()
                //access for logout
                .permitAll()
                        //url for logout
                .logoutUrl("/auth/logout")
                        //url page for success logout
                .logoutSuccessUrl("/")
                        //destroy session
                .invalidateHttpSession(true);

    }


}
