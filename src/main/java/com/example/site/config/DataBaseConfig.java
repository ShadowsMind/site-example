package com.example.site.config;

import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.Properties;

@Configuration
@EnableJpaRepositories(basePackages = "com.example.site.dao")
@EnableTransactionManagement
@PropertySource(value = "classpath:/configs/database.properties")
public class DataBaseConfig {

    private final String HIBERNATE_DIALECT  = "hibernate.dialect";
    private final String HIBERNATE_AUTO     = "hibernate.hbm2ddl.auto";
    private final String HIBERNATE_SHOW_SQL = "hibernate.show_sql";
    private final String ENTITIES_PATH      = "com.example.site.model.entity";

    @Autowired
    private Environment environment;

    @Bean(name = "dataSource")
    public DriverManagerDataSource dataSourceManager() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(environment.getProperty("db.driver"));
        dataSource.setUrl(environment.getProperty("db.url"));
        dataSource.setUsername(environment.getProperty("db.user"));
        dataSource.setPassword(environment.getProperty("db.password"));
        dataSource.setConnectionProperties(getConnectionProperties());

        return dataSource;
    }

    @Bean(name = "entityManagerFactory")
    public LocalContainerEntityManagerFactoryBean getEntityManager() {
        LocalContainerEntityManagerFactoryBean entityManager = new LocalContainerEntityManagerFactoryBean();

        entityManager.setDataSource(dataSourceManager());
        entityManager.setPersistenceProviderClass(HibernatePersistenceProvider.class);
        entityManager.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        entityManager.setJpaProperties(getHibernateProperties());
        entityManager.setPackagesToScan(ENTITIES_PATH);

        return entityManager;
    }

    @Autowired
    @Bean(name = "transactionManager")
    public JpaTransactionManager getTransactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(getEntityManager().getObject());

        return transactionManager;
    }

    private Properties getConnectionProperties() {
        Properties properties = new Properties();

        properties.put("useUnicode", "yes");
        properties.put("characterEncoding", "utf8");

        return properties;
    }

    private Properties getHibernateProperties() {
        Properties properties = new Properties();

        properties.put(HIBERNATE_SHOW_SQL, environment.getProperty("db.showsql"));
        properties.put(HIBERNATE_DIALECT, environment.getProperty("db.dialect"));
        properties.put(HIBERNATE_AUTO, environment.getProperty("db.hbm2ddl"));
        properties.put("hibernate.connection.characterEncoding", "UTF-8");
        properties.put("hibernate.connection.useUnicode", "true");
        properties.put("cache.provider_class", "org.hibernate.cache.internal.NoCachingRegionFactory");

        return properties;
    }

}
