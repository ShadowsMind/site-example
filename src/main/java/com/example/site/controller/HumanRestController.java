package com.example.site.controller;

import com.example.site.dao.HumanDao;
import com.example.site.model.dto.HumanDto;
import com.example.site.model.entity.Human;
import com.example.site.service.HumanRestService;
import com.example.site.service.HumanService;
import com.example.site.util.validation.HumanValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "api/humans", produces = "application/json")
public class HumanRestController {


    @Autowired
    HumanService humanService;

    @Autowired
    HumanValidation humanValidation;

    @Autowired
    HumanDao humanDao;

    @Autowired
    HumanRestService humanRestService;


    @RequestMapping(method = RequestMethod.GET)
    public List<Human> findAll() {
        return humanService.findAll();
    }

    @RequestMapping(method = RequestMethod.POST)
    public Map<String, ?> getHumanFromClient(@ModelAttribute HumanDto humanDto) {
        Map<String, Object> result = new HashMap<String, Object>();

        if (humanValidation.validate(humanDto)) {
            result.put("human", humanDto);
            result.put("status", "good");
        } else {
            result.put("status", "human not valid");
        }

        return result;
    }

    @RequestMapping(value = "/params", method = RequestMethod.POST)
    public HumanDto getHumanFromClientParams(@RequestParam String firstName,
                                             @RequestParam String lastName,
                                             @RequestParam Integer age) {
        return new HumanDto(null, firstName, lastName, age, null);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Human getAll(@PathVariable("id") Long id) {
        return humanDao.findOne(id);
    }

    @RequestMapping(value = "/browser", method = RequestMethod.GET)
    public String getBrowser(@RequestHeader(value="User-Agent") String userAgent) {
        return userAgent;
    }

    @RequestMapping(value = "/from-rest/{id}", method = RequestMethod.GET)
    public Human getHumanFromRest(@PathVariable("id") Long id) {
        return humanRestService.getById(id);
    }


}
