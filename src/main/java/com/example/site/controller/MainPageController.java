package com.example.site.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ViewResolver;

@Controller
public class MainPageController {

    @Autowired
    ViewResolver viewResolver;


    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView getMainPage() {
        ModelAndView modelAndView = new ModelAndView("main");
        modelAndView.addObject("title", "Main Page");
        modelAndView.addObject("body", "Hello World");

        return modelAndView;
    }

    @RequestMapping(value = "userdata", method = RequestMethod.POST)
    public ModelAndView getUserDataPage(@RequestParam String name, @RequestParam String email) {
        ModelAndView modelAndView = new ModelAndView("user_data");
        modelAndView.addObject("name", name);
        modelAndView.addObject("email", email);

        return modelAndView;
    }

    @RequestMapping(value = "userdata", method = RequestMethod.GET)
    public ModelAndView getUserDataPage2(@RequestParam String name, @RequestParam String email) {
        ModelAndView modelAndView = new ModelAndView("user_data");
        modelAndView.addObject("name", name);
        modelAndView.addObject("email", email);

        return modelAndView;
    }

    @RequestMapping(value = "auth", method = RequestMethod.GET)
    public ModelAndView getAuthPage() {
        return new ModelAndView("auth");
    }


    @RequestMapping(value = "profile", method = RequestMethod.GET)
    public ModelAndView getProfilePage() {
        return new ModelAndView("profile");
    }



}
