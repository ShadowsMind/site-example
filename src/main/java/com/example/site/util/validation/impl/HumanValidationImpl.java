package com.example.site.util.validation.impl;

import com.example.site.model.dto.HumanDto;
import com.example.site.util.validation.HumanValidation;
import org.springframework.stereotype.Component;

@Component
public class HumanValidationImpl implements HumanValidation {

    @Override
    public boolean validate(HumanDto humanDto) {
        if (humanDto.getFirstName().isEmpty()) {
            return false;
        }
        if (humanDto.getLastName().isEmpty()) {
            return false;
        }
        if (humanDto.getAge() < 7 || humanDto.getAge() > 80) {
            return false;
        }

        return true;
    }


}
