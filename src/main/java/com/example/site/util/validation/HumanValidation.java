package com.example.site.util.validation;

import com.example.site.model.dto.HumanDto;

public interface HumanValidation {

    public boolean validate(HumanDto humanDto);

}
