package com.example.site.service.impl;

import com.example.site.dao.HumanDao;
import com.example.site.model.entity.Human;
import com.example.site.service.HumanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.logging.Logger;

@Service
public class HumanServiceImpl implements HumanService {

    Logger logger = Logger.getLogger(HumanService.class.getName());

    @Autowired
    HumanDao humanDao;


    @Override
    public List<Human> findAll() {
        logger.info("Выполнен запрос в Базу данных на поиск всвех Людей");
        return (List<Human>) humanDao.findAll();
    }



}
