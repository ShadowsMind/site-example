package com.example.site.service.impl;

import com.example.site.model.dto.AccountDto;
import com.example.site.service.AccountRiotApiService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Service
public class AccountRiotApiServiceImpl implements AccountRiotApiService {

    private final String riotApiUrl = "https://euw.api.pvp.net/api/lol/euw/v1.4/summoner/by-name/{name}?api_key={apiKey}";
    private final String apiKey = "343826c1-0f9c-4877-8898-5b91b733a4f5";

    @Autowired
    RestTemplate restTemplate;

    @Override
    public Map<String, AccountDto> getAccountByName(String name) {

        Map<String, AccountDto> result = new HashMap<String, AccountDto>();

        Map<String, String> params = new HashMap<String, String>();
        params.put("name", name);
        params.put("apiKey", apiKey);

        String accountDto = restTemplate.getForObject(riotApiUrl, String.class, params);

        ObjectMapper objectMapper = new ObjectMapper();

        TypeReference<HashMap<String, AccountDto>> typeRef
                = new TypeReference<HashMap<String, AccountDto>>() {
        };

        try {
            result = objectMapper.readValue(accountDto, typeRef);
        } catch (IOException e) {
            e.printStackTrace();
        }


        return result;
    }
}
