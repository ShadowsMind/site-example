package com.example.site.service.impl;

import com.example.site.model.entity.Human;
import com.example.site.service.HumanRestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Service
public class HumanRestServiceImpl implements HumanRestService {

    private final String url = "http://localhost:8080/api/humans/";

    @Autowired
    RestTemplate restTemplate;


    @Override
    public Human getById(Long id) {
        String humanUrl = url + "{id}";

        Map<String, String> params = new HashMap<String, String>();
        params.put("id", id.toString());

        Human human = restTemplate.getForObject(humanUrl, Human.class, params);

        return human;
    }


}
