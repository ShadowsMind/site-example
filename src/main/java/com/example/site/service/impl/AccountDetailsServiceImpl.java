package com.example.site.service.impl;

import com.example.site.dao.AccountDao;
import com.example.site.model.entity.Account;
import com.example.site.model.entity.Role;
import com.example.site.service.AccountDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class AccountDetailsServiceImpl implements AccountDetailsService {

    @Autowired
    AccountDao accountDao;


    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        if (email != null && !email.isEmpty()) {
            Account account = accountDao.findOneByEmail(email);
            if (account == null) throw new UsernameNotFoundException("user with email not founded");

            List<GrantedAuthority> autorities = buildUserAuthority(account.getRoles());

            return buildUserForAuntefication(account, autorities);
        } else throw new UsernameNotFoundException("email is null");
    }

    private List<GrantedAuthority> buildUserAuthority(Set<Role> accountRoles) {
        Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();

        for(Role accountRole : accountRoles) {
            setAuths.add(new SimpleGrantedAuthority(accountRole.getName()));
        }

        return new ArrayList<GrantedAuthority>(setAuths);
    }

    private UserDetails buildUserForAuntefication(Account account, List<GrantedAuthority> authorities) {
        return new User(account.getEmail(), account.getPassword(), true, true, true, true, authorities);
    }


}
