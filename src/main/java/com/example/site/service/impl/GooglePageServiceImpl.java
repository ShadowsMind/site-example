package com.example.site.service.impl;

import com.example.site.service.GooglePageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class GooglePageServiceImpl implements GooglePageService {

    @Autowired
    RestTemplate restTemplate;


    @Override
    public String getGooglePage() {
        String googleUrl = "https://www.google.kz";
        return restTemplate.getForObject(googleUrl, String.class);
    }


}
