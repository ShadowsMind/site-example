package com.example.site.service;

import org.springframework.security.core.userdetails.UserDetailsService;

public interface AccountDetailsService extends UserDetailsService {
}
