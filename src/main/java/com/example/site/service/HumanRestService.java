package com.example.site.service;

import com.example.site.model.entity.Human;

public interface HumanRestService {

    public Human getById(Long id);

}
