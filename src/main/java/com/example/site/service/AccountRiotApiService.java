package com.example.site.service;

import com.example.site.model.dto.AccountDto;

import java.util.Map;

public interface AccountRiotApiService {

    public Map<String, AccountDto> getAccountByName(String name);

}
