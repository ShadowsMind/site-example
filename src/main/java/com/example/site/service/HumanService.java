package com.example.site.service;

import com.example.site.model.entity.Human;

import java.util.List;

public interface HumanService {

    public List<Human> findAll();

}
